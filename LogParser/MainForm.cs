﻿using LogParser.Models;
using LogParser.Utils;
using System;
using System.IO;
using System.Windows.Forms;

namespace LogParser
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void openFileButton_Click(object sender, EventArgs e)
        {
            resultList.Items.Clear();
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    var stream = dialog.OpenFile();
                    var reader = new StreamReader(stream);
                    var log = reader.ReadToEnd();
                    var parser = new Parser();
                    var items = parser.ParseDeviceParams(log);

                    if (items != null)
                    {
                        foreach (DeviceParams param in items)
                        {
                            resultList.Items.Add(param.ToItemOfList());
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error: " + ex.Message);
                }

            }
        }
    }
}
