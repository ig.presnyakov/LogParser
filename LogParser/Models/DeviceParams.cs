﻿using System.Windows.Forms;

namespace LogParser.Models
{
    class DeviceParams
    {
        public string Address;
        public string Code;

        public DeviceParams(string Address, string Code)
        {
            this.Address = Address;
            this.Code = Code;
        }

        public ListViewItem ToItemOfList()
        {
            return new ListViewItem(new[] { Address, Code });
        }
    }
}
