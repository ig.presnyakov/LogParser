﻿using LogParser.Models;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace LogParser.Utils
{
    class Parser
    {
        private const string LinePattern = ".+[|]+.+[|]+[a-zA-Z0-9 \\n\\r]+[|]+[a-zA-Z0-9 -]+";
        private const int NumberOfBlocksInLine = 4;
        private const int DeviceInfoBlockIndex = 2;
        private const char Separator = '|';
        private const string ParamsPattern = "(?<=7e).+?(?=7e )";
        private const int AddressLength = 8;
        private const int CommandCodeLength = 4;

        public List<DeviceParams> ParseDeviceParams(string paramsString)
        {
            var validLines = ParseValidLines(paramsString);
            var deviceRawParams = ParseDeviceBlock(validLines);
            return ParseDeviceParam(deviceRawParams);
        }

        //Parse lines with log pattern .|.|.|.
        private List<string> ParseValidLines(string str)
        {
            var exp = new Regex(LinePattern, RegexOptions.Multiline);
            var matches = new List<string>();

            foreach (string match in exp.Matches(str).Cast<Match>().Select(v => v.Value).ToList())
            {
                matches.Add(Regex.Replace(match, "\r\n", " "));
            }
            return matches;
        }

        //Parse block with params
        private List<string> ParseDeviceBlock(List<string> lines)
        {
            var deviceBlocks = new List<string>();

            foreach (string line in lines)
            {
                var splitedLine = line.Split(Separator);
                if (splitedLine.Length != NumberOfBlocksInLine)
                    continue;

                deviceBlocks.Add(splitedLine[DeviceInfoBlockIndex]);
            }

            return deviceBlocks;
        }

        //Parse DeviceParams
        private List<DeviceParams> ParseDeviceParam(List<string> deviceBlocks)
        {
            var paramExp = new Regex(ParamsPattern);
            var deviceParams = new List<DeviceParams>();

            foreach (string deviceBlock in deviceBlocks)
            {
                var targetBlock = paramExp.Matches(deviceBlock);

                foreach (Match match in targetBlock)
                {
                    if (!match.Success)
                        continue;

                    var value = match.Value.Replace(" ", string.Empty);
                    if (value.Length < CommandCodeLength + AddressLength)
                        continue;

                    var address = value.Substring(0, AddressLength);
                    var commandCode = value.Substring(AddressLength, CommandCodeLength);
                    deviceParams.Add(new DeviceParams(address, commandCode));
                }

            }

            return deviceParams;
        }
    }
}
